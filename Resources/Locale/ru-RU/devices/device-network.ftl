# named frequencies
device-frequency-prototype-name-atmos = Атмосферные приборы
device-frequency-prototype-name-suit-sensors = Сенсоры костюмов
device-frequency-prototype-name-crew-monitor = Монитор экипажа
device-frequency-prototype-name-lights = Умное освещение
device-frequency-prototype-name-mailing-units = Почтовый блок
device-frequency-prototype-name-pdas = КПК
device-frequency-prototype-name-fax = Факс

## camera frequencies
device-frequency-prototype-name-surveillance-camera-test = Тест подсети
device-frequency-prototype-name-surveillance-camera-engineering = Камеры инженеров
device-frequency-prototype-name-surveillance-camera-security = Камеры СБ
device-frequency-prototype-name-surveillance-camera-science = Камеры ученых
device-frequency-prototype-name-surveillance-camera-supply = Камеры снабжения
device-frequency-prototype-name-surveillance-camera-command = Камеры командования
device-frequency-prototype-name-surveillance-camera-service = Камеры сервиса
device-frequency-prototype-name-surveillance-camera-medical = Камеры врачей
device-frequency-prototype-name-surveillance-camera-general = Камеры обычные
device-frequency-prototype-name-surveillance-camera-entertainment = Камеры развлечения

# prefixes for randomly generated device addresses
device-address-prefix-vent = Вент-
device-address-prefix-scrubber = Скруб-
device-address-prefix-sensor = Сенс-

#PDAs and terminals
device-address-prefix-console = Конс-
device-address-prefix-fire-alarm = Пожар-
device-address-prefix-air-alarm = Возд-

device-address-examine-message = Адрес устройства: {$address}.

#Device net ID names
device-net-id-private = Частные
device-net-id-wired = Проводные
device-net-id-wireless = Беспроводные
device-net-id-apc = ЛКП
device-net-id-atmos-devices = Атмос Устройства
device-net-id-reserved = Резерв
